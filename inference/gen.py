from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, set_seed
import torch
import time
from diffusers import LCMScheduler, AutoPipelineForText2Image
import json
import csv
import os

generator = torch.Generator(device="cuda").manual_seed(2462436346)
model_id = "openchat/openchat_3.5"

quantization_config = BitsAndBytesConfig(
   load_in_4bit=True,
   bnb_4bit_compute_dtype=torch.bfloat16
)

tokenizer = AutoTokenizer.from_pretrained(model_id)
model = AutoModelForCausalLM.from_pretrained(
    model_id,
    quantization_config=quantization_config,
    device_map="auto",
    
).eval()

model_id = "stabilityai/stable-diffusion-xl-base-1.0"
adapter_id = "latent-consistency/lcm-lora-sdxl"

pipe = AutoPipelineForText2Image.from_pretrained(model_id, torch_dtype=torch.float16, variant="fp16",generator=generator)
pipe.scheduler = LCMScheduler.from_config(pipe.scheduler.config)


# load and fuse lcm lora

pipe.load_lora_weights(adapter_id)
pipe.load_lora_weights("/disk/ZaloChallenge2023/ComfyUI/models/loras/lora_sdxl_34000.safetensors")
pipe.to(device="cuda", dtype=torch.float16)
pipe.fuse_lora()

with open('/disk/ZaloChallenge2023/dataset/banner/test/info.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for i,row in enumerate(csv_reader):
        print(i)
        if i == 0:
            continue
        prompt = ""
        if os.path.exists(f'prompts/{row[0]}.txt'):
            f = open(f'prompts/{row[0]}.txt', "r")
            prompt = f.read()
        else:
            PROMPT = "GPT4 Correct User: {question}<|end_of_turn|>GPT4 Correct Assistant:"  
            advertise_info =  {
                "caption": row[1],
                "description": row[2],
                "moreInfo": row[3]
            }

            input_prompt = PROMPT.format_map(  
                {
                    "question": (
                        "The company has provided us with information about the product that we need to advertise"
                        f"{json.dumps(advertise_info, ensure_ascii=False, indent = 1)}\n"
                        "Do this step by step\n"
                        "1. Translate Vietnamese to English\n"
                        "2. Analyze text\n"
                        "3. Generate very detail text that decribe the banner image for product\n"
                    )
                }  
            )  
            input_ids = tokenizer(input_prompt, return_tensors="pt")  
            
            outputs = model.generate(  
                inputs=input_ids["input_ids"].to("cuda"),  
                attention_mask=input_ids["attention_mask"].to("cuda"),  
                do_sample=True,  
                temperature=0.5,  
                top_k=50,  
                top_p=0.5,
                max_new_tokens = 512
            )  
            
            response = tokenizer.batch_decode(outputs, skip_special_tokens=True)[0]  
            # print(response.split("\n")[-1])
            prompt = response.split("\n")[-1]
            with open(f'prompts/{row[0]}.txt', 'w') as f:
                f.write(prompt)
        neg_prompt = "blurry, poor quality" # Negative prompt here
        
        
        image = pipe(prompt=prompt, \
                    negative_prompt=neg_prompt, \
                    num_inference_steps=4, \
                    guidance_scale=1.5, \
                    height=536, \
                    width=1024, \
                    ).images[0]
        image = image.resize((1024,533))
        image = image.save(f"/disk/ZaloChallenge2023/banner-advertise/images/{row[4]}") 

