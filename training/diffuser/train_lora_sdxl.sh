export MODEL_NAME="stabilityai/stable-diffusion-xl-base-1.0"
export VAE_NAME="madebyollin/sdxl-vae-fp16-fix"
export TRAIN_DIR="/disk/ZaloChallenge2023/dataset/banner-dataset"
export OUTPUT_DIR="/disk/ZaloChallenge2023/Diffusion/diffusers/examples/text_to_image/models"
accelerate launch train_text_to_image_lora_sdxl.py \
  --pretrained_model_name_or_path=$MODEL_NAME \
  --pretrained_vae_model_name_or_path=$VAE_NAME \
  --dataset_name=lambdalabs/pokemon-blip-captions \
  --caption_column="text" \
  --resolution=512 --random_flip \
  --train_batch_size=4 \
  --num_train_epochs=100 --checkpointing_steps=500 \
  --learning_rate=1e-04 --lr_scheduler="constant" --lr_warmup_steps=0 \
  --mixed_precision="fp16" \
  --seed=42 \
  --validation_prompt="The image features a car covered with a white, protective cover. The car is parked on top of a sidewalk, which is lined with low stone walls. The cover fits the car quite well, covering it almost entirely save for a few small green stripes on the sides. The cover also has a pocket on one side, likely for holding the straps that help secure the cover on the car. The car appears to be parked in front of a row of green hedges, giving the impression that the car is protected not only from the elements but also from potential theft or vandalism. Overall, the image gives off a sense of organization and protection, showcasing the importance of proper car maintenance and care." --validation_epochs 3 \
  --output_dir=${OUTPUT_DIR} \

